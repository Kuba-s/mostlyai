package mostlyai.mostlyai.domain.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(Long id) {
        super("Entity not found for id: " + id);
    }

    public NotFoundException(String message) {
        super(message);
    }
}
