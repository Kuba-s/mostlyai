package mostlyai.mostlyai.domain;

import mostlyai.mostlyai.domain.exception.ValidationException;

public class DomainAssert {
    public static void notNull(Object value, String propertyName) {
        if (value == null) {
            throw new ValidationException(propertyName + " cannot be null");
        }
    }
}
