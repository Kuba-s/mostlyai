package mostlyai.mostlyai.domain.project.excpetion;

import mostlyai.mostlyai.domain.exception.NotFoundException;

public class ProjectEntryNotFoundException extends NotFoundException {
    public ProjectEntryNotFoundException(Long id) {
        super("Project entry not found for id: " + id);
    }
}
