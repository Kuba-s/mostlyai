package mostlyai.mostlyai.domain.project.excpetion;

import mostlyai.mostlyai.domain.exception.NotFoundException;

public class ProjectNotFoundException extends NotFoundException {
    public ProjectNotFoundException(Long id) {
        super("Project not found for id: " + id);
    }
}
