package mostlyai.mostlyai.domain.project;

import mostlyai.mostlyai.domain.project.dto.ProjectSummaryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface ProjectEntryRepository extends CrudRepository<ProjectEntry, Long> {
    List<ProjectEntry> findByProjectsId(Long id);

    @Query(value =
            "SELECT " +
                    "count(*) totalDays, " +
                    "COALESCE(SUM(time_spent), 0) totalTimeSpent, " +
                    "COALESCE(SUM(time_spent)/count(*), 0) averageTimeSpentPerDay " +
            "FROM " +
                    "(" +
                    "    SELECT " +
                    "        SUM(time_spent) time_spent " +
                    "    FROM " +
                    "        project_entries" +
                    "    WHERE " +
                    "        projects_id = :projectsId" +
                    "    GROUP BY" +
                    "        entry_date" +
                    ") AS timeSpentSums"
            , nativeQuery = true)
    ProjectSummaryDTO getProjectSummary(@Param("projectsId") Long projectId);

    @Query(value =
            "SELECT " +
                    "COALESCE(SUM(time_spent), 0) time_spent " +
            "FROM " +
                    "project_entries " +
            "WHERE " +
                    "projects_id = :projectsId " +
            "AND " +
                    "entry_date = :entryDate"

            , nativeQuery = true)
    Double getSpentTimeForProjectAndDate(Long projectsId, LocalDate entryDate);

    @Query(value =
            "SELECT " +
                    "count(*) " +
            "FROM " +
                    "project_entries " +
            "WHERE " +
                    "projects_id = :projectsId " +
            "AND " +
                    "entry_date > :entryDate"
            , nativeQuery = true)
    Integer getEntriesAmountAfterDateForProject(Long projectsId, LocalDate entryDate);


    @Query(value =
            "SELECT " +
                    "count(*) " +
                    "FROM " +
                    "project_entries " +
                    "WHERE " +
                    "projects_id = :projectsId " +
                    "AND " +
                    "entry_date < :entryDate"
            , nativeQuery = true)
    Integer getEntriesAmountBeforeDateForProject(Long projectsId, LocalDate entryDate);
}
