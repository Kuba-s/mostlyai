package mostlyai.mostlyai.domain.project;

import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.DomainAssert;
import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.command.AddProjectCommand;
import mostlyai.mostlyai.domain.project.command.UpdateProjectCommand;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = true)
    private LocalDate endDate;

    private Project() {
    }

    public Project(String name, LocalDate startDate, LocalDate endDate) {
        validateDates(startDate, endDate);
        DomainAssert.notNull(name, "Name");
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public static Project of(AddProjectCommand projectDTO) {
        return new Project(
                projectDTO.getName(),
                projectDTO.getStartDate(),
                projectDTO.getEndDate()
        );
    }

    public ProjectDTO toDto() {
        return new ProjectDTO(
            id,
            name,
            startDate,
            endDate
        );
    }

    public void update(UpdateProjectCommand command) {
        validateDates(command.getStartDate(), command.getEndDate());
        DomainAssert.notNull(name, "Name");
        this.name = command.getName();
        this.startDate = command.getStartDate();
        this.endDate = command.getEndDate();
    }

    private void validateDates(LocalDate startDate, LocalDate endDate) {
        DomainAssert.notNull(startDate, "Start date");
        if (endDate != null && startDate.isAfter(endDate)) {
            throw new ValidationException("Start date cannot be after end date");
        }
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Boolean hasEndDate() {
        return endDate != null;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
