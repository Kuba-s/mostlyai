package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.dto.ProjectEntryDTO;
import mostlyai.mostlyai.domain.CommandHandler;
import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

@Component
public class AddProjectEntryCommandHandler implements CommandHandler<AddProjectEntryCommand, ProjectEntryDTO> {

    private final ProjectRepository projectRepository;
    private final ProjectEntryRepository projectEntryRepository;

    public AddProjectEntryCommandHandler(ProjectRepository projectRepository, ProjectEntryRepository projectEntryRepository) {
        this.projectRepository = projectRepository;
        this.projectEntryRepository = projectEntryRepository;
    }

    @Override
    public ProjectEntryDTO handle(AddProjectEntryCommand command) {
        Project project = projectRepository.findById(command.getProjectId()).orElseThrow(() -> new ProjectNotFoundException(command.getProjectId()));
        validateEntryDate(command.getEntryDate(), project);
        Double totalSpentTime = projectEntryRepository.getSpentTimeForProjectAndDate(command.getProjectId(), command.getEntryDate());
        ProjectEntry.validateTimeSpent(totalSpentTime + command.getTimeSpent());
        ProjectEntry projectEntry = ProjectEntry.of(command);

        projectEntry = projectEntryRepository.save(projectEntry);

        return projectEntry.toDto();
    }

    private void validateEntryDate(LocalDate entryDate, Project project) {
        if (entryDate.isBefore(project.getStartDate())) {
            throw new ValidationException("Entry date cannot be before project start date. Entry date: " + entryDate.toString() + ", project start date: " + project.getStartDate().toString());
        }

        if (project.hasEndDate() && entryDate.isAfter(project.getEndDate())) {
            throw new ValidationException("Entry date cannot be after project end date. Entry date: " + entryDate.toString() + ", project end date: " + project.getEndDate().toString());
        }
    }
}
