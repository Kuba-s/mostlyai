package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.Command;
import java.time.LocalDate;

public class AddProjectCommand implements Command {
    private final String name;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public AddProjectCommand(String name, LocalDate startDate, LocalDate endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
