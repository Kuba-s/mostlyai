package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.CommandHandler;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import org.springframework.stereotype.Component;

@Component
public class AddProjectCommandHandler implements CommandHandler<AddProjectCommand, ProjectDTO> {

    private final ProjectRepository projectRepository;

    public AddProjectCommandHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public ProjectDTO handle(AddProjectCommand command) {
        Project project = projectRepository.save(Project.of(command));
        return project.toDto();
    }
}
