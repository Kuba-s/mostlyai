package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.Command;

import java.time.LocalDate;

public class AddProjectEntryCommand implements Command {
    private final Long projectId;
    private final String description;
    private final LocalDate entryDate;
    private final Double timeSpent;

    public AddProjectEntryCommand(Long projectId, LocalDate entryDate, Double timeSpent, String description) {
        this.projectId = projectId;
        this.entryDate = entryDate;
        this.timeSpent = timeSpent;
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    public Double getTimeSpent() {
        return timeSpent;
    }
}
