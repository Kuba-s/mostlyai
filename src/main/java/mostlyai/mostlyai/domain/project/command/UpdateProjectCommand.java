package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.Command;

import java.time.LocalDate;

public class UpdateProjectCommand implements Command {
    private final Long id;
    private final String name;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public UpdateProjectCommand(Long id, String name, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Boolean hasEndDate() {
        return endDate != null;
    }
}
