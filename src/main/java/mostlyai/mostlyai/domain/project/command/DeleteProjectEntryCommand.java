package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.Command;

public class DeleteProjectEntryCommand implements Command {
    private final Long id;
    private final Long projectId;

    public DeleteProjectEntryCommand(Long id, Long projectId) {
        this.id = id;
        this.projectId = projectId;
    }

    public Long getId() {
        return id;
    }

    public Long getProjectId() {
        return projectId;
    }
}
