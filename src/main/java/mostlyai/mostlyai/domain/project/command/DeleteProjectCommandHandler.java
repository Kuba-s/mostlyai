package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.CommandHandler;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class DeleteProjectCommandHandler implements CommandHandler<DeleteProjectCommand, Boolean> {

    private final ProjectRepository projectRepository;

    public DeleteProjectCommandHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Boolean handle(DeleteProjectCommand command) {
        Project project = projectRepository.findById(command.getId()).orElseThrow(() -> new ProjectNotFoundException(command.getId()));
        projectRepository.delete(project);
        return Boolean.TRUE;
    }
}
