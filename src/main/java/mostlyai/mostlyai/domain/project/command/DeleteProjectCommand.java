package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.Command;

public class DeleteProjectCommand implements Command {
    private final Long id;

    public DeleteProjectCommand(String id) {
        this.id = Long.parseLong(id);
    }

    public Long getId() {
        return id;
    }

}
