package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.CommandHandler;
import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UpdateProjectCommandHandler implements CommandHandler<UpdateProjectCommand, ProjectDTO> {

    private final ProjectRepository projectRepository;
    private final ProjectEntryRepository projectEntryRepository;

    public UpdateProjectCommandHandler(ProjectRepository projectRepository, ProjectEntryRepository projectEntryRepository) {
        this.projectRepository = projectRepository;
        this.projectEntryRepository = projectEntryRepository;
    }

    @Override
    public ProjectDTO handle(UpdateProjectCommand command) {
        Project project = projectRepository.findById(command.getId()).orElseThrow(() -> new ProjectNotFoundException(command.getId()));
        validateStartDateChange(project, command);
        validateEndDateChange(project, command);
        project.update(command);
        return project.toDto();
    }

    private void validateEndDateChange(Project project, UpdateProjectCommand command) {
        if (project.hasEndDate()
                && command.hasEndDate()
                && command.getEndDate().isBefore(project.getEndDate())
                && 0 < projectEntryRepository.getEntriesAmountAfterDateForProject(command.getId(), command.getEndDate())
        ) {
            throw new ValidationException("Project has entries after " + command.getEndDate());
        }
    }

    private void validateStartDateChange(Project project, UpdateProjectCommand command) {
        if (command.getStartDate().isAfter(project.getStartDate())
                && 0 < projectEntryRepository.getEntriesAmountBeforeDateForProject(command.getId(), command.getStartDate())
        ) {
            throw new ValidationException("Project has entries before " + command.getStartDate());
        }
    }
}
