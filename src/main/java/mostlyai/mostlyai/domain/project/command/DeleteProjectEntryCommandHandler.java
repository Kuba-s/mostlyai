package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.CommandHandler;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectEntryNotFoundException;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class DeleteProjectEntryCommandHandler implements CommandHandler<DeleteProjectEntryCommand, Boolean> {

    private final ProjectRepository projectRepository;
    private final ProjectEntryRepository projectEntryRepository;

    public DeleteProjectEntryCommandHandler(ProjectRepository projectRepository, ProjectEntryRepository projectEntryRepository) {
        this.projectRepository = projectRepository;
        this.projectEntryRepository = projectEntryRepository;
    }

    @Override
    public Boolean handle(DeleteProjectEntryCommand command) {
        projectRepository.findById(command.getProjectId()).orElseThrow(() -> new ProjectNotFoundException(command.getProjectId()));
        ProjectEntry projectEntry = projectEntryRepository.findById(command.getId()).orElseThrow(() -> new ProjectEntryNotFoundException(command.getId()));
        projectEntryRepository.delete(projectEntry);
        return Boolean.TRUE;
    }
}
