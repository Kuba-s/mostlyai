package mostlyai.mostlyai.domain.project;

import mostlyai.mostlyai.domain.project.dto.ProjectEntryDTO;
import mostlyai.mostlyai.domain.DomainAssert;
import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.command.AddProjectEntryCommand;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "project_entries")
public class ProjectEntry {
    public final static Double MAX_TIME_SPENT_PER_DAY = 10.0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private Long projectsId;

    @Column(nullable = false)
    private LocalDate entryDate;

    @Column(nullable = false, columnDefinition = "NUMERIC")
    private Double timeSpent;

    @Column(nullable = true)
    private String description;

    private ProjectEntry() {
        // serialize
    }

    public ProjectEntry(Long projectsId, LocalDate entryDate, Double timeSpent, String description) {
        DomainAssert.notNull(projectsId, "Project id");
        DomainAssert.notNull(entryDate, "Entry date");
        validateTimeSpent(timeSpent);
        this.projectsId = projectsId;
        this.entryDate = entryDate;
        this.timeSpent = timeSpent;
        this.description = description;
    }

    public ProjectEntryDTO toDto() {
        return new ProjectEntryDTO(
                id,
                entryDate,
                timeSpent,
                description
        );
    }

    public static ProjectEntry of(AddProjectEntryCommand command) {
        return new ProjectEntry(
            command.getProjectId(),
            command.getEntryDate(),
            command.getTimeSpent(),
            command.getDescription()
        );
    }

    public static void validateTimeSpent(Double timeSpent) {
        DomainAssert.notNull(timeSpent, "Time spent");
        if (timeSpent < 0) {
            throw new ValidationException("Maximum time spent must be greater than 0");
        }
        if (timeSpent > MAX_TIME_SPENT_PER_DAY) {
            throw new ValidationException("Maximum time spent in one day cannot be greater than " + MAX_TIME_SPENT_PER_DAY);
        }
    }
}
