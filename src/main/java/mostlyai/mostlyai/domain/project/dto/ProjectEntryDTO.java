package mostlyai.mostlyai.domain.project.dto;

import mostlyai.mostlyai.domain.Command;

import java.time.LocalDate;

public class ProjectEntryDTO implements Command {
    private final Long id;
    private final String description;
    private final Double timeSpent;
    private final LocalDate entryDate;

    public ProjectEntryDTO(Long id, LocalDate entryDate, Double timeSpent, String description) {
        this.id = id;
        this.description = description;
        this.timeSpent = timeSpent;
        this.entryDate = entryDate;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    public Double getTimeSpent() {
        return timeSpent;
    }
}
