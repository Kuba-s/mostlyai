package mostlyai.mostlyai.domain.project.dto;

public interface ProjectSummaryDTO {
    public Integer getTotalDays();
    public Double getTotalTimeSpent();
    public Double getAverageTimeSpentPerDay();
}
