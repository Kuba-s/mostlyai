package mostlyai.mostlyai.domain.project.query;

import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetProjectListQueryHandler {
    private final ProjectRepository projectRepository;

    public GetProjectListQueryHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<ProjectDTO> execute() {
        List<Project> projectList = (List<Project>) projectRepository.findAll();
        List<ProjectDTO> projectDTOList = projectList.stream().map(Project::toDto).collect(Collectors.toList());

        return projectDTOList;
    }
}
