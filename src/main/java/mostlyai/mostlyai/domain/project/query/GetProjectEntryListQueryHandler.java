package mostlyai.mostlyai.domain.project.query;

import mostlyai.mostlyai.domain.project.dto.ProjectEntryDTO;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetProjectEntryListQueryHandler {
    private final ProjectRepository projectRepository;
    private final ProjectEntryRepository projectEntryRepository;

    public GetProjectEntryListQueryHandler(ProjectRepository projectRepository, ProjectEntryRepository projectEntryRepository) {
        this.projectRepository = projectRepository;
        this.projectEntryRepository = projectEntryRepository;
    }

    public List<ProjectEntryDTO> execute(Long id) {
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));
        List<ProjectEntry> projectEntryList = projectEntryRepository.findByProjectsId(id);
        List<ProjectEntryDTO> projectEntryDTOList = projectEntryList.stream().map(ProjectEntry::toDto).collect(Collectors.toList());

        return projectEntryDTOList;
    }
}
