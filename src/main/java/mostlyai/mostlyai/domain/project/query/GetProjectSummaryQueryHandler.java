package mostlyai.mostlyai.domain.project.query;

import mostlyai.mostlyai.domain.project.dto.ProjectSummaryDTO;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class GetProjectSummaryQueryHandler {
    private final ProjectRepository projectRepository;
    private final ProjectEntryRepository projectEntryRepository;

    public GetProjectSummaryQueryHandler(ProjectRepository projectRepository, ProjectEntryRepository projectEntryRepository) {
        this.projectRepository = projectRepository;
        this.projectEntryRepository = projectEntryRepository;
    }

    public ProjectSummaryDTO execute(Long id) {
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));
        ProjectSummaryDTO projectSummary = projectEntryRepository.getProjectSummary(id);

        return projectSummary;
    }
}
