package mostlyai.mostlyai.domain.project.query;

import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class GetProjectQueryHandler {
    private final ProjectRepository projectRepository;

    public GetProjectQueryHandler(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectDTO execute(Long id) {
        Project project = projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

        return project.toDto();
    }
}
