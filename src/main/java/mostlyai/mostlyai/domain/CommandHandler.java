package mostlyai.mostlyai.domain;

public interface CommandHandler<C extends Command, R> {
    public R handle(C command);
}
