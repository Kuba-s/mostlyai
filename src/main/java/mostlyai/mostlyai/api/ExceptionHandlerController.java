package mostlyai.mostlyai.api;

import mostlyai.mostlyai.api.resources.ErrorResponse;
import mostlyai.mostlyai.domain.exception.NotFoundException;
import mostlyai.mostlyai.domain.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandlerController {
    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(value = Throwable.class)
    protected ResponseEntity<?> handleProjectNotFoundException(Throwable ex, HttpServletRequest httpRequest) {
        log.error("Thrown exception:, {}", ex);

        if (ex instanceof NotFoundException) {
            ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), httpRequest.getRequestURI());
            return new ResponseEntity(error, HttpStatus.NOT_FOUND);
        } else if (ex instanceof ValidationException) {
            ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), httpRequest.getRequestURI());
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

        ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), httpRequest.getRequestURI());
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}