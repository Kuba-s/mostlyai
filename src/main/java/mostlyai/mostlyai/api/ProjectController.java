package mostlyai.mostlyai.api;

import mostlyai.mostlyai.api.resources.AddProjectEntryRequestBody;
import mostlyai.mostlyai.domain.project.dto.ProjectSummaryDTO;
import mostlyai.mostlyai.domain.project.dto.ProjectDTO;
import mostlyai.mostlyai.domain.project.dto.ProjectEntryDTO;
import mostlyai.mostlyai.api.resources.UpdateProjectRequestBody;
import mostlyai.mostlyai.domain.project.command.AddProjectCommand;
import mostlyai.mostlyai.domain.project.command.AddProjectCommandHandler;
import mostlyai.mostlyai.domain.project.command.AddProjectEntryCommandHandler;
import mostlyai.mostlyai.domain.project.command.DeleteProjectCommand;
import mostlyai.mostlyai.domain.project.command.DeleteProjectCommandHandler;
import mostlyai.mostlyai.domain.project.command.DeleteProjectEntryCommand;
import mostlyai.mostlyai.domain.project.command.DeleteProjectEntryCommandHandler;
import mostlyai.mostlyai.domain.project.command.UpdateProjectCommandHandler;
import mostlyai.mostlyai.domain.project.query.GetProjectEntryListQueryHandler;
import mostlyai.mostlyai.domain.project.query.GetProjectListQueryHandler;
import mostlyai.mostlyai.domain.project.query.GetProjectQueryHandler;
import mostlyai.mostlyai.domain.project.query.GetProjectSummaryQueryHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ProjectController {

    final private AddProjectCommandHandler addProjectCommandHandler;
    final private UpdateProjectCommandHandler updateProjectCommandHandler;
    final private GetProjectListQueryHandler getProjectListQueryHandler;
    final private GetProjectQueryHandler getProjectQueryHandler;
    final private DeleteProjectCommandHandler deleteProjectCommand;
    final private GetProjectEntryListQueryHandler getProjectEntryListQueryHandler;
    final private AddProjectEntryCommandHandler addProjectEntryCommandHandler;
    final private DeleteProjectEntryCommandHandler deleteProjectEntryCommandHandler;
    final private GetProjectSummaryQueryHandler getProjectSummaryQueryHandler;

    public ProjectController(AddProjectCommandHandler addProjectCommandHandler, UpdateProjectCommandHandler updateProjectCommandHandler, GetProjectListQueryHandler getProjectListQueryHandler, GetProjectQueryHandler getProjectQueryHandler, DeleteProjectCommandHandler deleteProjectCommand, GetProjectEntryListQueryHandler getProjectEntryListQueryHandler, AddProjectEntryCommandHandler addProjectEntryCommandHandler, DeleteProjectEntryCommandHandler deleteProjectEntryCommandHandler, GetProjectSummaryQueryHandler getProjectSummaryQueryHandler) {
        this.addProjectCommandHandler = addProjectCommandHandler;
        this.updateProjectCommandHandler = updateProjectCommandHandler;
        this.getProjectListQueryHandler = getProjectListQueryHandler;
        this.getProjectQueryHandler = getProjectQueryHandler;
        this.deleteProjectCommand = deleteProjectCommand;
        this.getProjectEntryListQueryHandler = getProjectEntryListQueryHandler;
        this.addProjectEntryCommandHandler = addProjectEntryCommandHandler;
        this.deleteProjectEntryCommandHandler = deleteProjectEntryCommandHandler;
        this.getProjectSummaryQueryHandler = getProjectSummaryQueryHandler;
    }

    @GetMapping(value = "/projects")
    public ResponseEntity<List<ProjectDTO>> getProjectList() {
        return ResponseEntity.ok(getProjectListQueryHandler.execute());
    }

    @GetMapping(value = "/projects/{projectId}")
    public ResponseEntity<ProjectDTO> getProject(
            @PathVariable Long projectId
    ) {
        return ResponseEntity.ok(getProjectQueryHandler.execute(projectId));
    }

    @PostMapping(value = "/projects")
    @Valid
    public ResponseEntity<ProjectDTO> addProject(
            @RequestBody AddProjectCommand command
    ) {
        return ResponseEntity.of(Optional.of(
                addProjectCommandHandler.handle(command)
        ));
    }

    @PutMapping(value = "/projects/{projectId}")
    public ResponseEntity<ProjectDTO> updateProject(
            @RequestBody UpdateProjectRequestBody requestBody,
            @PathVariable Long projectId
    ) {
        return ResponseEntity.of(Optional.of(
                updateProjectCommandHandler.handle(requestBody.toCommand(projectId))
        ));
    }

    @DeleteMapping(value = "/projects/{command}")
    public ResponseEntity deleteProject(
            @PathVariable DeleteProjectCommand command
    ) {
        deleteProjectCommand.handle(command);
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @GetMapping(value = "/projects/{projectId}/entries")
    public ResponseEntity<List<ProjectEntryDTO>> getProjectEntriesList(
            @PathVariable Long projectId
    ) {
        return ResponseEntity.ok(getProjectEntryListQueryHandler.execute(projectId));
    }

    @PostMapping(value = "/projects/{projectId}/entries")
    public ResponseEntity<ProjectEntryDTO> addProjectEntriesList(
            @RequestBody AddProjectEntryRequestBody requestBody,
            @PathVariable Long projectId
    ) {
        return ResponseEntity.of(Optional.of(
                addProjectEntryCommandHandler.handle(requestBody.toCommand(projectId))
        ));
    }

    @DeleteMapping(value = "/projects/{projectId}/entries/{id}")
    public ResponseEntity deleteProjectEntriesList(
            @PathVariable Long id,
            @PathVariable Long projectId
    ) {
        deleteProjectEntryCommandHandler.handle(new DeleteProjectEntryCommand(id, projectId));
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @GetMapping(value = "/projects/{projectId}/summary")
    public ResponseEntity<ProjectSummaryDTO> getProjectSummary(
            @PathVariable Long projectId
    ) {
        return ResponseEntity.ok(getProjectSummaryQueryHandler.execute(projectId));
    }
}
