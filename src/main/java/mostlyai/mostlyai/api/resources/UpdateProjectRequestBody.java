package mostlyai.mostlyai.api.resources;

import mostlyai.mostlyai.domain.project.command.UpdateProjectCommand;

import java.time.LocalDate;

public class UpdateProjectRequestBody {
    private final String name;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public UpdateProjectRequestBody(String name, LocalDate startDate, LocalDate endDate) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public UpdateProjectCommand toCommand(Long projectId) {
        return new UpdateProjectCommand(
                projectId,
                name,
                startDate,
                endDate
        );
    }
}
