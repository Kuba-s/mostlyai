package mostlyai.mostlyai.api.resources;

import mostlyai.mostlyai.domain.project.command.AddProjectEntryCommand;

import java.time.LocalDate;

public class AddProjectEntryRequestBody {

    private final String description;
    private final LocalDate entryDate;
    private final Double timeSpent;

    public AddProjectEntryRequestBody(String description, LocalDate entryDate, Double timeSpent) {
        this.description = description;
        this.entryDate = entryDate;
        this.timeSpent = timeSpent;
    }

    public AddProjectEntryCommand toCommand(Long projectId) {
        return new AddProjectEntryCommand(
                projectId,
                entryDate,
                timeSpent,
                description
        );
    }
}
