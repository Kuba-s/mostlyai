CREATE TABLE projects
(
    id BIGSERIAL CONSTRAINT projects_pk PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE
);

CREATE TABLE project_entries
(
    id BIGSERIAL CONSTRAINT project_entries_pk PRIMARY KEY,
    projects_id BIGINT NOT NULL CONSTRAINT project_entries_projects_id_fk REFERENCES projects ON DELETE CASCADE,
    entry_date DATE NOT NULL,
    time_spent DECIMAL(5, 2) NOT NULL,
    description VARCHAR(256)
);

CREATE INDEX project_entries_projects_id_index ON project_entries (projects_id);
CREATE INDEX project_entries_projects_id_entry_date_index ON project_entries (projects_id, entry_date);
