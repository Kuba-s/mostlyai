package mostlyai.mostlyai.domain.project;

import mostlyai.mostlyai.domain.exception.ValidationException;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProjectTest {

    @Test
    public void shouldCreateProject() {
        new Project(
                "some name",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
    }

    @Test
    public void shouldThrowExceptionWhenEndDateIsBeforeStartDate() {
        assertThrows(ValidationException.class, () -> {
            new Project(
                "some name",
                LocalDate.now(),
                LocalDate.now().minusDays(1L)
            );
        });
    }
}
