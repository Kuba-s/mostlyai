package mostlyai.mostlyai.domain.project;

import mostlyai.mostlyai.domain.exception.ValidationException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static mostlyai.mostlyai.domain.project.ProjectEntry.MAX_TIME_SPENT_PER_DAY;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProjectEntryTest {
    @Test
    public void shouldCreateProjectEntry() {
        new ProjectEntry(
            1L,
            LocalDate.now(),
            2.0,
            "some description"
        );
    }

    @Test
    public void shouldThrowExceptionWhenTimeSpentIsTooBig() {
        assertThrows(ValidationException.class, () -> {
            ProjectEntry projectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                MAX_TIME_SPENT_PER_DAY + 1.0,
                "some description"
            );
        });
    }

    @Test
    public void shouldThrowExceptionWhenTimeSpentIsLessThanZero() {
        assertThrows(ValidationException.class, () -> {
            ProjectEntry projectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                -1.0,
                "some description"
            );
        });
    }
}
