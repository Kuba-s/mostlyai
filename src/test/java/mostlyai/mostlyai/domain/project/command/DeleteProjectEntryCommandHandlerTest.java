package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DeleteProjectEntryCommandHandlerTest {

    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private ProjectEntryRepository projectEntryRepository;

    private DeleteProjectEntryCommandHandler handler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        handler = new DeleteProjectEntryCommandHandler(projectRepository, projectEntryRepository);
    }

    @Test
    public void shouldDeleteProjectEntry() {
        DeleteProjectEntryCommand command = new DeleteProjectEntryCommand(1L, 2L);
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        ProjectEntry projectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                1.1,
                "some description"
        );

        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.findById(anyLong())).thenReturn(Optional.of(projectEntry));
        doNothing().when(projectRepository).delete(any());
        handler.handle(command);
    }

    @Test
    public void shouldThrowExceptionWhenDeletingNotExistingProject() {
        DeleteProjectEntryCommand command = new DeleteProjectEntryCommand(1L, 2L);
        when(projectRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ProjectNotFoundException.class, () -> {
            handler.handle(command);
        });
    }


    @Test
    public void shouldThrowExceptionWhenDeletingNotExistingProjectEntry() {
        DeleteProjectEntryCommand command = new DeleteProjectEntryCommand(1L, 2L);
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(projectEntryRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ProjectNotFoundException.class, () -> {
            handler.handle(command);
        });
    }
}
