package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DeleteProjectCommandHandlerTest {

    @Mock
    private ProjectRepository projectRepository;

    private DeleteProjectCommandHandler handler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        handler = new DeleteProjectCommandHandler(projectRepository);
    }

    @Test
    public void shouldDeleteProject() {
        DeleteProjectCommand command = new DeleteProjectCommand("1");
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        doNothing().when(projectRepository).delete(any());
        handler.handle(command);
    }

    @Test
    public void shouldThrowExceptionWhenDeletingNotExistingProject() {
        DeleteProjectCommand command = new DeleteProjectCommand("1");
        when(projectRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ProjectNotFoundException.class, () -> {
            handler.handle(command);
        });
    }
}
