package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.dto.ProjectEntryDTO;
import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntry;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class AddProjectEntryCommandHandlerTest {

    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private ProjectEntryRepository projectEntryRepository;

    private AddProjectEntryCommandHandler handler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        handler = new AddProjectEntryCommandHandler(projectRepository, projectEntryRepository);
    }

    @Test
    public void shouldAddProjectEntry() {
        AddProjectEntryCommand command = new AddProjectEntryCommand(
                1L,
                LocalDate.now(),
                1.1,
                "some description"
        );
        Project project = new Project(
            "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        ProjectEntry projectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                1.1,
                "some description"
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.getSpentTimeForProjectAndDate(anyLong(), any())).thenReturn(0.0);
        when(projectEntryRepository.save(any())).thenReturn(projectEntry);
        handler.handle(command);
    }


    @Test
    public void shouldAddTimeToExistingProjectEntry() {
        AddProjectEntryCommand command = new AddProjectEntryCommand(
                1L,
                LocalDate.now(),
                1.1,
                "some description2"
        );

        Double initialTimeSpent = 1.1;

        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        ProjectEntry projectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                1.1,
                "some description"
        );

        ProjectEntry expectedProjectEntry = new ProjectEntry(
                1L,
                LocalDate.now(),
                2.2,
                "some description"
        );

        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.getSpentTimeForProjectAndDate(anyLong(), any())).thenReturn(initialTimeSpent);
        when(projectEntryRepository.save(any())).thenReturn(projectEntry);
        ProjectEntryDTO returnedProjectEntry = handler.handle(command);
        assertEquals(expectedProjectEntry.toDto().getDescription(), returnedProjectEntry.getDescription());
        assertEquals(expectedProjectEntry.toDto().getEntryDate(), returnedProjectEntry.getEntryDate());
        assertEquals(expectedProjectEntry.toDto().getId(), returnedProjectEntry.getId());
        assertEquals(expectedProjectEntry.toDto().getTimeSpent(), returnedProjectEntry.getTimeSpent() + initialTimeSpent);
    }

    @Test
    public void shouldThrowExceptionWhenEntryDateIsBeforeProjectStartDate() {
        AddProjectEntryCommandHandler handler = new AddProjectEntryCommandHandler(projectRepository, projectEntryRepository);
        AddProjectEntryCommand command = new AddProjectEntryCommand(
                1L,
                LocalDate.now().minusDays(2L),
                1.1,
                "some description"
        );
        Project project = new Project(
                "projectName",
                LocalDate.now().minusDays(1L),
                LocalDate.now().plusDays(1L)
        );

        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.getSpentTimeForProjectAndDate(anyLong(), any())).thenReturn(0.0);
        assertThrows(ValidationException.class, () -> {
            handler.handle(command);
        });
    }
}
