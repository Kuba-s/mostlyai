package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@Component
public class AddProjectCommandHandlerTest {

    @Mock
    private ProjectRepository projectRepository;

    private AddProjectCommandHandler handler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        handler = new AddProjectCommandHandler(projectRepository);
    }

    @Test
    public void shouldAddProject() {
        AddProjectCommand command = new AddProjectCommand(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        when(projectRepository.save(any())).thenReturn(project);
        handler.handle(command);
    }
}
