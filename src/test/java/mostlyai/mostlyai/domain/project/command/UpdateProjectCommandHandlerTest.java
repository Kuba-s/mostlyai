package mostlyai.mostlyai.domain.project.command;

import mostlyai.mostlyai.domain.exception.ValidationException;
import mostlyai.mostlyai.domain.project.Project;
import mostlyai.mostlyai.domain.project.ProjectEntryRepository;
import mostlyai.mostlyai.domain.project.ProjectRepository;
import mostlyai.mostlyai.domain.project.excpetion.ProjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@Component
public class UpdateProjectCommandHandlerTest {

    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private ProjectEntryRepository projectEntryRepository;

    private UpdateProjectCommandHandler handler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        handler = new UpdateProjectCommandHandler(projectRepository, projectEntryRepository);
    }

    @Test
    public void shouldUpdateProject() {
        UpdateProjectCommand command = new UpdateProjectCommand(
                3L,
                "projectName2",
                LocalDate.now().plusDays(1L),
                LocalDate.now().plusDays(2L)
        );
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        Project updatedProject = new Project(
                "projectName2",
                LocalDate.now().plusDays(1L),
                LocalDate.now().plusDays(2L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectRepository.save(any())).thenReturn(project);
        handler.handle(command);
    }

    @Test
    public void shouldThrowExceptionWhenUpdatingNotExistingProject() {
        UpdateProjectCommand command = new UpdateProjectCommand(
                2L,
                "projectName2",
                LocalDate.now().plusDays(1L),
                LocalDate.now().plusDays(2L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ProjectNotFoundException.class, () -> {
            handler.handle(command);
        });
    }

    @Test
    public void shouldThrowExceptionWhenChangeStartDateToLaterWhichCollidesWithEntriesDate() {
        UpdateProjectCommand command = new UpdateProjectCommand(
                1L,
                "projectName2",
                LocalDate.now().plusDays(1L),
                LocalDate.now().plusDays(2L)
        );
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(1L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.getEntriesAmountBeforeDateForProject(anyLong(), any())).thenReturn(1);
        assertThrows(ValidationException.class, () -> {
            handler.handle(command);
        });    }


    @Test
    public void shouldThrowExceptionWhenChangeEndDateToEarlierWhichCollidesWithEntriesDate() {
        UpdateProjectCommand command = new UpdateProjectCommand(
                1L,
                "projectName2",
                LocalDate.now().plusDays(1L),
                LocalDate.now().plusDays(2L)
        );
        Project project = new Project(
                "projectName",
                LocalDate.now(),
                LocalDate.now().plusDays(3L)
        );
        when(projectRepository.findById(anyLong())).thenReturn(Optional.of(project));
        when(projectEntryRepository.getEntriesAmountAfterDateForProject(anyLong(), any())).thenReturn(1);
        assertThrows(ValidationException.class, () -> {
            handler.handle(command);
        });
    }
}
